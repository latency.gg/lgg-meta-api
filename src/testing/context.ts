import * as metaApi from "@latency.gg/lgg-meta-oas";
import * as http from "http";
import * as prom from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

interface Context {
    endpoints: {
        application: URL,
    },
    servers: {
        application: application.Server,
    },
    createApplicationClient(): metaApi.Client,
}

export async function withContext(
    job: (context: Context) => Promisable<void>,
) {
    const endpoints = {
        application: new URL("http://localhost:9000"),
    };

    const abortController = new AbortController();
    const promRegistry = new prom.Registry();

    const applicationConfig: application.Config = {
        endpoint: endpoints.application,

        livenessController: abortController,
        readinessController: abortController,

        promRegistry,
    };

    const onError = (error: unknown) => {
        if (!abortController.signal.aborted) {
            throw error;
        }
    };

    const applicationContext = application.createContext(
        applicationConfig,
    );

    const servers = {
        application: application.createServer(
            applicationContext,
            onError,
        ),
    };

    const httpServers = {
        application: http.createServer(servers.application.asRequestListener({
            onError,
        })),
    };

    const context = {
        endpoints,
        servers,
        createApplicationClient() {
            return new metaApi.Client(
                {
                    baseUrl: endpoints.application,
                },
            );
        },
    };

    const keys = Object.keys(httpServers) as Array<keyof typeof httpServers>;
    await Promise.all(
        keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
            endpoints[key].port,
            () => resolve(),
        ))),
    );

    try {
        await job(context);
    }
    finally {
        abortController.abort();

        await Promise.all(
            keys.map(async key => new Promise<void>(
                (resolve, reject) => httpServers[key].close(
                    error => error ?
                        reject(error) :
                        resolve(),
                )),
            ),
        );
    }
}
